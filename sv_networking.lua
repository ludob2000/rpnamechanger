--#NoSimplerr#
local config = RPN.config
util.AddNetworkString("RPN:OpenRPNameSetter")
util.AddNetworkString("RPN:SetPlayerInfo")

local function getFile()
  return util.JSONToTable(file.Read(config.path) or "{}") or {}
end

--Check if player has set a RP name
hook.Add("PlayerInitialSpawn", "checkPlayerRPName", function(ply)
  timer.Simple(2, function()
    local f = getFile()
    if f[ply:SteamID()] then --Setting playermodel
      ply:SetModel(f[ply:SteamID()])
    end

    if not ply:getDarkRPVar("rpname") or not f[ply:SteamID()] then --Check if rpname is set.
      net.Start("RPN:OpenRPNameSetter")
      net.Send(ply)
    end
  end)
end)

net.Receive("RPN:SetPlayerInfo", function(len, ply)
  local name = net.ReadString()
  local model = net.ReadString()

  --Setting RP name
  DarkRP.storeRPName(ply, name)
  ply:setDarkRPVar("rpname", name)

  --Setting RP model
  local f = getFile()
  f[ply:SteamID()] = model
  file.Write(config.path, util.TableToJSON(f,true))

  ply:SetModel(model)
end)
