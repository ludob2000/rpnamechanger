--#NoSimplerr#
local config = RPN.config

local function openRPNameSetter()
	local frame = vgui.Create("DFrame")
	frame:SetSize(550, ScrH()-450)
	frame:SetTitle("")
	frame:MakePopup()
	frame:SetAlpha(0)
	frame:ShowCloseButton(false)
	function frame:Paint(w,h)
		draw.RoundedBox(4,0,0,w,h,config.menus.bodyColor)
	end
	function frame:Think()
		self:Center()
	end
	function frame:OnKeyCodePressed(key)
		if key ==  KEY_F1 then self.titleBar.close:DoClick() end
	end

	local ow,oh = frame:GetSize()
	frame:SetSize(ow/2, oh/2)
	frame:AlphaTo(255,0.25)
	frame:SizeTo(ow, oh, 0.2, 0, -0.5, function()
		--Title bar
		frame.titleBar = frame:Add("DPanel")
		frame.titleBar:SetSize(frame:GetWide(), 40)
		function frame.titleBar:Paint(w,h)
			draw.RoundedBoxEx(4,0,0,w,h,config.menus.titleBarColor,true,true)
		end

		local function get_scale()
			local sc = math.Clamp(ScrH() / 1080, 0.75, 1)
			if (!th) then
				th = 48 * sc
				m = th * 0.25
			end

			return sc
		end

		--Close Button
		frame.titleBar.close = frame.titleBar:Add("DButton")
		frame.titleBar.close:SetText("")
		frame.titleBar.close:SetFont("SH_REPORTS.Largest")
		frame.titleBar.close:SetColor(color_white)
		frame.titleBar.close:SetSize(frame.titleBar:GetTall(),frame.titleBar:GetTall())
		frame.titleBar.close:SetPos(frame.titleBar:GetWide()-frame.titleBar.close:GetWide(),0)
    frame.titleBar.close:SetVisible(false)
		local matClose = Material("shenesis/general/close.png", "noclamp smooth")
		local scale = get_scale()
		function frame.titleBar.close:Paint(w,h)
			if self:IsHovered() then
				draw.RoundedBoxEx(4, 0, 0, w, h, config.menus.closeBtnHoverColor)
			else
				draw.RoundedBoxEx(4, 0, 0, w, h, config.menus.closeBtnNoHoverColor)
			end

			surface.SetDrawColor(color_white)
			surface.SetMaterial(matClose)
			surface.DrawTexturedRectRotated(w * 0.5, h * 0.5, 16, 16, 0)
		end
		function frame.titleBar.close:DoClick()
			frame:AlphaTo(0,0.1)
			frame:SizeTo(frame:GetWide()-100,frame:GetTall()-100,0.1,0,0.5,function()
				frame:Remove()
			end)
		end

		--Panel Title
		frame.titleBar.title = frame.titleBar:Add("DLabel")
		frame.titleBar.title:SetText(config.menus.title)
		frame.titleBar.title:SetFont(config.menus.titleFont)
		frame.titleBar.title:SetColor(color_white)
		frame.titleBar.title:SetPos(10,0)
		frame.titleBar.title:SizeToContents()
		frame.titleBar.title:CenterVertical()

		local function modelChooser(name)
			local scroll = frame:Add("DScrollPanel") --Creating list and displaying models
			scroll:SetSize(frame:GetWide()-frame.mdl:GetWide(), frame:GetTall()-frame.titleBar:GetTall()-40-(frame.titleBar:GetTall()+frame.hint:GetTall()))
			scroll:SetPos(frame.mdl:GetWide(), frame.titleBar:GetTall()+frame.hint:GetTall()+30)
			local list = scroll:Add("DIconLayout")
			list:SetSize(scroll:GetSize())
      list:SetSpaceX(2)
      list:SetSpaceY(2)
      list:SetSize(scroll:GetWide()-list:GetSpaceX()*2-25, scroll:GetTall()-list:GetSpaceY()*2)
      list:SetPos(list:GetSpaceX(), list:GetSpaceY())
			for k,v in pairs(config.models) do
				local mp = list:Add("DPanel")
				mp:SetSize(list:GetWide()/4-list:GetSpaceX(), list:GetTall()/4-list:GetSpaceY())
				function mp:Paint(w,h)
					draw.RoundedBox(4, 0, 0, w, h, Color(160,160,160,20))
				end

        timer.Simple(0.5, function()
          mp.mdl = mp:Add("DModelPanel")
          mp.mdl:SetModel(v)
          mp.mdl:Dock(FILL)
          mp.mdl:SetFOV(20)
          function mp.mdl:LayoutEntity(ent)
            local head = ent:LookupBone("ValveBiped.Bip01_Head1")
            if head and head > 1 then
              self:SetLookAt(ent:GetBonePosition(head))
            end
            ent:SetAngles(Angle(0,80,0))
          end
          function mp.mdl:DoClick()
            frame.mdl:SetModel(v)
          end
        end)
			end

			local finish = frame:Add("DButton")
			finish:SetText("Finish")
			finish:SetColor(Color(255,255,255))
			finish:SetSize(frame:GetWide()-frame.mdl:GetWide()-25, 40)
      local x,y = scroll:GetPos()
			finish:SetPos(x+list:GetSpaceX(), frame:GetTall()-finish:GetTall()-10)
			function finish:Paint(w,h)
				if self:IsHovered() then
					draw.RoundedBox(4,0,0,w,h,Color(160,160,160,30))
				else
          draw.RoundedBox(4,0,0,w,h,Color(160,160,160,20))
				end
			end
      function finish:DoClick()
        net.Start("RPN:SetPlayerInfo")
          net.WriteString(name)
          net.WriteString(frame.mdl:GetModel())
        net.SendToServer()

        frame.titleBar.close:DoClick()
      end

			--Fadein new children
			finish:SetAlpha(0)
			scroll:SetAlpha(0)
			finish:AlphaTo(255,0.3,0.5)
			scroll:AlphaTo(255,0.3,0.5)
      for k,pnl in pairs(list:GetChildren()) do
        pnl:SetAlpha(0)
        pnl:AlphaTo(255,0.3,0.5+(0.01*k))
      end
		end

		frame.mdl = frame:Add("DModelPanel")
		frame.mdl:SetSize(frame:GetWide()*0.30, frame:GetTall()-frame.titleBar:GetTall())
		frame.mdl:SetPos(0, frame.titleBar:GetTall())
		frame.mdl:SetModel(config.models[1])
		frame.mdl:SetFOV(30)
		function frame.mdl:LayoutEntity(ent)
      ent:SetAngles(Angle(0,45,0))
    end

		local function styleTE(te)
			function te:Paint(w,h)
				draw.RoundedBox(4,0,0,w,h,Color(255,255,255))

        if self:GetText() == self.placeholder then
          self:DrawTextEntryText(config.menus.textEntryPlaceholderCol, config.menus.titleBarColor, Color(0,0,0))
        else
          self:DrawTextEntryText(Color(0,0,0), config.menus.titleBarColor, Color(0,0,0))
        end
			end

			te.placeholder = te:GetText()
			function te:Think()
				local val = self:GetText()
				if self:IsEditing() and val == self.placeholder then
					self:SetText("")
				end

				if not self:IsEditing() and #val == 0 then
					self:SetText(self.placeholder)
				end
			end
		end

		local form = frame:Add("DPanel")
		form.fn = form:Add("DTextEntry") --Firstname
		form.fn:SetText("Firstname")
		form.fn:SetSize(frame:GetWide()-frame.mdl:GetWide()-100,40)
    function form.fn:OnEnter()
      form.ln:RequestFocus()
    end
		form.ln = form:Add("DTextEntry") --Lastname
		form.ln:SetText("Lastname")
		form.ln:SetSize(frame:GetWide()-frame.mdl:GetWide()-100,40)
		form.ln:SetPos(0,form.fn:GetTall()+10)
		styleTE(form.fn)
		styleTE(form.ln)

		form:SizeToChildren(true, true)
		form:CenterHorizontal(0.6)
		form:CenterVertical(0.4)
		function form:Paint() end

    frame.hint = frame:Add("DLabel")
		frame.hint:SetText(config.menus.nameHintText)
		frame.hint:SetFont(config.menus.hintFont)
		frame.hint:SetColor(Color(255,255,255))
    function frame.hint:PerformLayout(w,h)
      self:SizeToContents()
      local x,y = form:GetPos()
			self:SetPos(x,frame.titleBar:GetTall()+20)
    end

		frame.finish = frame:Add("DButton")
		frame.finish:SetText("Continue")
		frame.finish:SetSize(form:GetWide(),30)
		frame.finish:SetColor(Color(255,255,255))
		frame.finish.color = Color(160,160,160,20)
		function frame.finish:SetColor(col) self.color = col end
		function frame.finish:GetColor() return self.color end
		function frame.finish:Paint(w,h)
			if not self:GetDisabled() then
				if self:IsHovered() then
					self.color = Color(160,160,160,30)
				else
					self.color = Color(160,160,160,20)
				end
			end
			draw.RoundedBox(4, 0, 0, w, h, self.color)
		end
		function frame.finish:Think()
      local x,y = form:GetPos()
			self:SetPos(x,y+form:GetTall()+10)
		end
		function frame.finish:Flash(txt, time)
			self.ot = self:GetText()

			self:SetText(txt)
			self:SetDisabled(true)
			self:ColorTo(Color(225,75,75), 0.3)

			self:ColorTo(Color(160,160,160,20), 0.3, time, function()
				self:SetText(self.ot)
        self:SetDisabled(false)
			end)
		end
		function frame.finish:DoClick()
			local fn = form.fn:GetText()
			local ln = form.ln:GetText()

			if fn == "" or ln == "" or fn == form.fn.placeholder or ln == form.ln.placeholder then
				self:Flash("You need to enter both first and last names", 3)
			else
				frame.hint:AlphaTo(0, 0.2, 0, function() --Smoothly change hint text
					frame.hint:SetText(config.menus.appHintText)
					frame.hint:AlphaTo(255,0.5,0.2)
				end)

				--Fade out other panels
				form:AlphaTo(0,0.4,0)
				self:AlphaTo(0,0.4,0,function()
					form:Remove()
					self:Remove()
				end)

				modelChooser(fn .. " " .. ln)
			end
		end

		--Fade in all children
		for _,pnls in pairs(frame:GetChildren()) do
			pnls:SetAlpha(0)
			pnls:AlphaTo(255,0.2)
		end
	end)
end

concommand.Add("RPN", function(ply)
  if not ply:IsSuperAdmin() or not ply:IsAdmin() then
    return
  end
	openRPNameSetter()
end)

net.Receive("RPN:OpenRPNameSetter", openRPNameSetter)
