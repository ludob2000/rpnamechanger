--#NoSimplerr#
local config = RPN.config
hook.Add("CanChangeRPName", "CheckIfVIP_RMNAME", function(ply)
  for k,v in pairs(config.rpnameUserGroups) do
    if ply:IsUserGroup(v) then
      return true
    end
  end

  return false
end)

if SERVER then
  DarkRP.defineChatCommand("forcerpname", function(ply, args)
    if not ply:IsAdmin() or not ply:IsSuperAdmin() then
      ply:ChatPrint("You do not have access to this command.")
      return ""
    end

    local target = DarkRP.findPlayer(args[1])
    local name = table.concat(args," ", 2)

    if not target then
      ply:ChatPrint("The target player was not found")
      return ""
    end

    net.Start("RPN:OpenRPNameSetter")
    net.Send(target)

    ply:ChatPrint(config.greeted)
    return ""
  end)
end

DarkRP.declareChatCommand {
  command = "forcerpname",
  description = "Enables the target player to reconfigure their character",
  delay = 5
}
