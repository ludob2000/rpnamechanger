--#NoSimplerr#
RPN = {}


--General settings
local config = {}
config.menus = {}
config.menus.title = "Set Roleplay Name" --The title of the menu
config.menus.titleFont = "SH_REPORTS.Larger" --The font used by the title
config.menus.hintFont = "Trebuchet24"
config.menus.titleBarColor = Color(50,153,220) --The title bar color
config.menus.bodyColor = Color(39,60,79) --The overall body color
config.menus.closeBtnHoverColor = Color(231, 76, 60, 255)
config.menus.closeBtnNoHoverColor = Color(44, 62, 80, 150)

config.menus.textEntryPlaceholderCol = Color(187,187,187)

config.menus.nameHintText = "Choose your character name"
config.menus.appHintText = "Choose your appearance"

config.greeted = "The target was greeted with the character configuration menu."

config.path = "playerrpmodels.txt"
config.models = {
  "models/player/alyx.mdl",
  "models/player/group01/female_01.mdl",
  "models/player/group01/female_02.mdl",
  "models/player/group01/female_03.mdl",
  "models/player/group01/female_04.mdl",
  "models/player/group01/female_06.mdl",
  "models/player/group01/male_01.mdl",
  "models/player/group01/male_02.mdl",
  "models/player/group01/male_03.mdl",
  "models/player/group01/male_04.mdl",
  "models/player/group01/male_05.mdl",
  "models/player/group01/male_06.mdl",
  "models/player/group01/male_07.mdl",
  "models/player/group01/male_08.mdl",
  "models/player/group01/male_09.mdl"
}
config.rpnameUserGroups = { --The usergroups able to change their rpname
  "donator"
}

RPN.config = config
